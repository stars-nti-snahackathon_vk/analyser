# `Stars` team training code for [SNAhackathon 2016 dataset][snahackathon-dataset-desc]

## Bootstrapping

```lang:sh
git clone $THIS_REPO

pip install -r requirements.txt

jupyter notebook
```

### Downloading data

Unfortunately, SNAhackathon does not provide a direct link to their dataset, therefore we can't just `curl` it. You have to download it manually from https://cloud.mail.ru/public/GtHV/JNYJbuTV1 and (assuming you've placed it into repo root):

```lang:sh
unzip SNAHackaton2016.zip

mv SNAHackaton2016 data
mv data/trainGraph data/graph

rm SNAHackaton2016.zip
```

[snahackathon-dataset-desc]: http://snahackathon.org/dataset
